<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class StudentTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/student')
                ->maximize()
                ->type('studentname', 'Kennedy Mutisya')
                ->type('admno', '4590')
                ->select('house', 'KIPLAGAT')
                ->select('classroom', '1B')
                ->type('kcpe', '473')
                ->type('mobilenumber', '0713642175')
                ->press('Add Student')
                ->pause(5000);
        });
    }
}
