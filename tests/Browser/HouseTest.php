<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class HouseTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/house')
                ->type('house', 'KIPLAGAT')
                ->press('Add House')
//                ->pause(6000)
                ->type('house', 'MUSUVA')
                ->press('Add House')
                ->pause(5000);
        });
    }
}
