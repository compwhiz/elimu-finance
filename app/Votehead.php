<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Votehead
 *
 * @property int $id
 * @property string $voteheadid
 * @property string $votehead
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votehead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votehead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votehead query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votehead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votehead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votehead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votehead whereVotehead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votehead whereVoteheadid($value)
 * @mixin \Eloquent
 */
class Votehead extends Model
{
    //
}
