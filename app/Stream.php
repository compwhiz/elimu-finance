<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Stream
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stream newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stream newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stream query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stream whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stream whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stream whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Stream extends Model
{
    //
}
