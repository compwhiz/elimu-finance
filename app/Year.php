<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Year
 *
 * @property int $id
 * @property string $year
 * @property int $current
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Year newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Year newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Year query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Year whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Year whereCurrent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Year whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Year whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Year whereYear($value)
 * @mixin \Eloquent
 */
class Year extends Model
{
    //
}
