<?php

namespace App\Http\Controllers;

use App\Voteheas;
use Illuminate\Http\Request;

class VoteheasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voteheas  $voteheas
     * @return \Illuminate\Http\Response
     */
    public function show(Voteheas $voteheas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voteheas  $voteheas
     * @return \Illuminate\Http\Response
     */
    public function edit(Voteheas $voteheas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voteheas  $voteheas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voteheas $voteheas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voteheas  $voteheas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voteheas $voteheas)
    {
        //
    }
}
