<?php

namespace App\Http\Controllers;

use App\Term;
use Illuminate\Http\Request;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Term $term)
    {
        $years = $term::all();
        return view('Registration.term', compact('years'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function store(Request $request, Term $term)
    {
        $count = Term::all()->where('term', $request->term)->where('year', $request->year)->count();
        $this->validate($request, [
            'year' => ['required', 'digits:4'],
            'term' => ['required', function ($attribute, $value, $fail) use ($request) {
                $count = Term::all()->where('term', $value)->where('year', $request->year)->count();
                if ($count >= 1) {
                    $fail('This Term already exists. Kindly add another.');
                }
            }],
        ]);

        $term->term = $request->term;
        $term->year = $request->year;
        $term->saveOrFail();

        return response()->json($term);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Term $term
     * @return \Illuminate\Http\Response
     */
    public function show(Term $term)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Term $term
     * @return \Illuminate\Http\Response
     */
    public function edit(Term $term)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Term $term
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Term $term)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Term $term
     * @return \Illuminate\Http\Response
     */
    public function destroy(Term $term)
    {
        //
    }
}
