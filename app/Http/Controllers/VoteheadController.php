<?php

namespace App\Http\Controllers;

use App\Votehead;
use Illuminate\Http\Request;

class VoteheadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Votehead $votehead)
    {
        $voteheads = $votehead::all();
        return view('Registration.votehead',compact('voteheads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Votehead $votehead)
    {
        $this->validate($request, [
            'voteheadid' => ['required'],
            'votehead'   => ['required','unique:voteheads,votehead']
        ]);

        $votehead->voteheadid = $request->voteheadid;
        $votehead->votehead = $request->votehead;
        $votehead->saveOrFail();

        return response()->json($votehead);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Votehead $votehead
     * @return \Illuminate\Http\Response
     */
    public function show(Votehead $votehead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Votehead $votehead
     * @return \Illuminate\Http\Response
     */
    public function edit(Votehead $votehead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Votehead $votehead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Votehead $votehead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Votehead $votehead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Votehead $votehead)
    {
        //
    }
}
