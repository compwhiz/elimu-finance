<?php

namespace App\Http\Controllers;

use App\Votehead;
use App\Year;
use App\Yearvotehead;
use Illuminate\Http\Request;

class YearvoteheadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Year $year, Votehead $votehead, Yearvotehead $yearvotehead)
    {
        $years = $year::all();
        $voteheads = $votehead::all();
        $yearvoteheads = $yearvotehead::with(['votehead','year'])->get()->groupBy('year');
//        dd($yearvoteheads);
        return view('settings.yearvoteheads', compact('years', 'voteheads','yearvoteheads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Yearvotehead $votehead)
    {
        $this->validate($request, [

        ]);
        $votehead->votehead_id = $request->votehead;
        $votehead->year = $request->year;
        $votehead->saveorFail();
        return response()->json($votehead);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Yearvotehead $yearvotehead
     * @return \Illuminate\Http\Response
     */
    public function show(Yearvotehead $yearvotehead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Yearvotehead $yearvotehead
     * @return \Illuminate\Http\Response
     */
    public function edit(Yearvotehead $yearvotehead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Yearvotehead $yearvotehead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Yearvotehead $yearvotehead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Yearvotehead $yearvotehead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Yearvotehead $yearvotehead)
    {
        //
    }
}
