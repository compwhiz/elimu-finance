<?php

namespace App\Http\Controllers;

use App\Feestructure;
use Illuminate\Http\Request;

class FeestructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Feestructure $feestructure)
    {
        $this->validate($request, [
            'year'=> ['required'],
            'form'=> ['required'],
            'terms.*.termone' => ['required','numeric'],
            'terms.*.termtwo' => 'required',
            'terms.*.termthree' => 'required',
        ]);

        collect($request->terms)->each(function ($data) use ($request) {
            $fee = new Feestructure();

            $fee->year_id = $request->year;
            $fee->votehead_id = $data['voteheadid'];
            $fee->form_id = $request->form;
            $fee->termone = $data['termone'];
            $fee->termtwo = $data['termtwo'];
            $fee->termthree = $data['termthree'];
            $fee->saveOrFail();
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feestructure  $feestructure
     * @return \Illuminate\Http\Response
     */
    public function show(Feestructure $feestructure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feestructure  $feestructure
     * @return \Illuminate\Http\Response
     */
    public function edit(Feestructure $feestructure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feestructure  $feestructure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feestructure $feestructure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feestructure  $feestructure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feestructure $feestructure)
    {
        //
    }
}
