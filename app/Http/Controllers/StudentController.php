<?php

namespace App\Http\Controllers;

use App\Form;
use App\House;
use App\Student;
use App\Term;
use App\Year;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(House $house, Term $term, Form $form, Student $student)
    {
        return view('Registration.students', [
            'house' => $house::all(),
            'term'  => $term::all(),
            'form'  => $form::all(),
            'student'  => $student::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Student $student)
    {
        $this->validate($request, [
            'studentname'  => ['required'],
            'admno'        => ['required'],
            'house'        => ['required'],
            'classroom'    => ['required'],
            'kcpe'         => ['required'],
            'mobilenumber' => ['required'],
        ]);
        $year = Year::all()->where('current',1)->first();
        $student->name = $request->studentname;
        $student->admno = $request->admno;
        $student->house_id = $request->house;
        $student->form_id = $request->classroom;
        $student->year_id = $year->id ?? 0;
        $student->kcpe = $request->kcpe;
        $student->mobilenumber = $request->mobilenumber;
        $student->saveOrFail();

        return response()->json($student);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Student $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}
