@extends('layouts.app')

@section('content')
    <yearvoteheads :years="{{ $years }}" :voteheads="{{ $voteheads}}"></yearvoteheads>
    <div class="mt-5">

        <yearvoteheadsview :years="{{ $yearvoteheads }}" :mia="{{ \App\Year::all() }}"></yearvoteheadsview> {{--<houses :house="{{$house}}"></houses>--}}
    </div>
@endsection
